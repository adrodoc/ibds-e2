import unittest
from ibds.e2 import kaya


class KayaTestCase(unittest.TestCase):
    def test_germany(self):
        actual = kaya.calc_kaya_identity(pop=82.4, gdp=44, enInt=5, carbInt=0.05)
        self.assertAlmostEqual(actual, 906.4)

    def test_negative_pop(self):
        with self.assertRaisesRegex(ValueError, "pop \\(-82.4\\) is negative"):
            kaya.calc_kaya_identity(pop=-82.4, gdp=44, enInt=5, carbInt=0.05)

    def test_negative_gdp(self):
        with self.assertRaisesRegex(ValueError, "gdp \\(-44\\) is negative"):
            kaya.calc_kaya_identity(pop=82.4, gdp=-44, enInt=5, carbInt=0.05)

    def test_negative_enInt(self):
        with self.assertRaisesRegex(ValueError, "enInt \\(-5\\) is negative"):
            kaya.calc_kaya_identity(pop=82.4, gdp=44, enInt=-5, carbInt=0.05)

    def test_negative_carbInt(self):
        with self.assertRaisesRegex(ValueError, "carbInt \\(-0.05\\) is negative"):
            kaya.calc_kaya_identity(pop=82.4, gdp=44, enInt=5, carbInt=-0.05)

    def test_germany_c(self):
        actual = kaya.calc_kaya_identity(pop=82.4, gdp=44, enInt=5, carbInt=0.05, output="C")
        self.assertAlmostEqual(actual, 246.97547683923)
