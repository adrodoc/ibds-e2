def calc_kaya_identity(pop: float, gdp: float, enInt: float, carbInt: float, output: str = "CO2"):
    """
    Calculate the Kaya identity (yearly emissions in million tons).

    :param pop: Population size (in millions).
    :param gdp: GDP per capita (in 1000$/person).
    :param enInt: Energy Intensity (in Gigajoule/$1000GDP). Energy Intensity is the energy needed to produce a certain amount of economic value.
    :param carbInt: Carbon Intensity (in tonnes CO2/Gigajoule). Carbon Intensity is the CO2 emitted for produced energy. This number depends on the energy mix used (coal, solar, ...).
    :param output: The type of emissions that should be calculated. "CO2" (the default) for million tons of carbon dioxide or "C" for million tons of carbon.
    :return: The Kaya Identity can be computed as: pop * gdp * enInt * carbInt
    """
    if pop < 0: raise ValueError("pop ({}) is negative".format(pop))
    if gdp < 0: raise ValueError("gdp ({}) is negative".format(gdp))
    if enInt < 0: raise ValueError("enInt ({}) is negative".format(enInt))
    if carbInt < 0: raise ValueError("carbInt ({}) is negative".format(carbInt))
    if output not in ["C", "CO2"]: raise ValueError("output ({}) must be 'C' or 'CO2'".format(output))
    result = pop * gdp * enInt * carbInt
    if output == "C":
        result /= 3.67
    return result
